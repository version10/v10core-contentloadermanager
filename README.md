# ContentLoaderManager #
![Release](https://img.shields.io/badge/release-v3.0.4-green.svg)

The aim of this project is to provide a easy lightweight module to load dynamic contents. List of items display in a container.

## Features

* easy to use
* super lightweight
* Load contents list of items.
* Load more button option with auto hide.
* custom parameters

# Dependencies

* jQuery

# Installing

* `bower install https://bitbucket.org/version10/v10core-contentloadermanager.git --save`

## Using

```javascript
    var clm = new V10Core.ContentLoaderManager({OPTIONS});
```

## Options

Option | Type | Default | Description
------------ | ------ | --------- | -------------
urlService | String | `null` | url service to get contents
limit | Number  | `10` | Number of item to load
container | jQuery Array Object  | `null` | Container that contain items
order | String  | `asc` | asc or desc display direction
btnLoadMore | jQuery Object  | `null` | Button to load more items
parametersUrl | Object  | `{}` | List of custum parameters of urlService
autoLoadOnStart | Boolean  | `true` | Call the urlService on start
callback | Function  | `null` | Callback after getting content with return data

## Examples ##

### FrontEnd

```javascript
    jQuery(document).ready(function($) {
        var clm = new V10Core.ContentLoaderManager({
            urlService:'MY_SERVICE_URL.php',
            container:[$('[data-my-container-left]'), $('[data-my-container-right]')],
            btnLoadMore:$('[data-btn-loadmore]'),
            parametersUrl:{type:'custom'},
            callback:function(data){
                //CODE HERE
            }
        });
    });
```

### BackEnd

```PHP
    public function getProjects(Application $app, Request $request)
    {
        $limit = (int)$request->get('limit', 0);
        $offset = (int)$request->get('offset', 0);

        $productions = $app['production.service']->getProductions($offset, $limit);

        $htmlArray = [];

        foreach ($productions['rows'] as $production) {
            $htmlArray[] = $app['twig']->render('inc/ajax/more-projects.html.twig', [
                'production' => $production
            ]);
        }
        $issetMore = $productions['isset_more'];

        return $app->json(array(
            'html' => $htmlArray,
            'isset_more' => $issetMore
        ));
    }
```

## Using with Isotope ##

### Isotope Dependencies

* Isotope `bower install isotope --save`
* Imagesloaded `bower install imagesloaded --save`

#### Base HTML Grid

```HTML

    <div data-my-container class="grid">
        <div class="grid-sizer"></div>
        <div class="gutter-sizer"></div>
    </div>

```

#### HTML Content

```HTML

    <div class="characters-block grid-item" data-iso>
        //...your content inner grid item
    </div>

```

#### Styles

```LESS

    .grid-sizer{
        width: 48%;
        @media @mobile{
            width: 100%;
        }
    }
    .gutter-sizer {
        width: 1%;
        @media @mobile{
            width: 0%;
        }
    }
    .grid-item{
        width: 48%;

        @media @mobile{
            width: 100%;
        }
    }

```

#### JS Declaration of isotope content and code inner the callback

```javascript

    var isotopeContent = null;

    jQuery...

            callback:function(data){
                if(isotopeContent == null){
                    isotopeContent = $('[data-my-container]').isotope({
                        itemSelector: '.grid-item',
                        masonry: {
                            columnWidth: '.grid-sizer',
                            gutter: '.gutter-sizer'
                        },
                        percentPosition: true
                    });
                }else{
                    isotopeContent.isotope( 'appended', $('[data-my-container] [data-iso]'));
                }

                $('[data-my-container] [data-iso]').removeAttr("data-iso");

                isotopeContent.imagesLoaded().progress( function() {
                    isotopeContent.isotope('layout');
                });

                 isotopeContent.isotope('layout');

                 //YOUR CUSTOM CODE HERE...

            }

        ...
```



### Credits ###

- Michael Chartrand (SirKnightDragoon)
- Maxime Thibault
