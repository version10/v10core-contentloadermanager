//ContentLoaderManager v3.0.4
if (typeof V10Core === "undefined") V10Core = {};

V10Core.ContentLoaderManager = function (options) {
    var _this = this;
    this.offset = 0;

    this.options = {
        urlService: null,
        limit: 10,
        container: null,
        order: 'asc',
        btnLoadMore: null,
        parametersUrl: {},
        autoLoadOnStart: true,
        callback:null
    };

    for (o in options) {
        this.options[o] = options[o];
    }

    if (this.options.autoLoadOnStart == true) {
        this.loadMore();
    }

    this.options.btnLoadMore.on('click', function (e) {
        e.preventDefault();

        _this.loadMore();
    });
};

V10Core.ContentLoaderManager.prototype.loadMore = function () {
    var _this = this;
    var data = {};

    for (var elem in this.options.parametersUrl) {
        data[elem] = this.options.parametersUrl[elem];
    }

    data.limit = this.options.limit;
    data.offset = this.offset;
    _this.options.btnLoadMore.hide();

    $.ajax({
        method: "GET",
        url: this.options.urlService,
        data: data
    }).done(function (datas) {

        for (var i = 0; i < datas.html.length; i+=_this.options.container.length){
            for (var i2 = 0; i2 < _this.options.container.length; i2++){
                if (_this.options.order == 'asc') {
                    _this.options.container.eq(i2).append(datas.html[(i+i2)]);
                } else {
                    _this.options.container.eq(i2).prepend(datas.html[(i+i2)]);
                }
            }
        }

        _this.offset += _this.options.limit;

        if (datas.isset_more == true) {
            _this.options.btnLoadMore.show();
        }

        if(_this.options.callback != null) _this.options.callback(datas);
    });
};

V10Core.ContentLoaderManager.prototype.version = "3.0.4";
